declare module "*.svg" {
    import type m from "mithril";
    const vnode: m.Vnode<any, any>;
    export default vnode;
}
