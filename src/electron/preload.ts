import { contextBridge, ipcRenderer } from "electron";
import { PreloadData } from "../common/PreloadData";

function getFlashVersion(): string | null {
    if (navigator.plugins) {
        const plugin = navigator.plugins.namedItem?.("Shockwave Flash");
        if (plugin) {
            return plugin.description;
        }
    }
    return null;
}

const { port1: dealer, port2: router } = new MessageChannel();
ipcRenderer.postMessage("router", null, [router]);

contextBridge.exposeInMainWorld("etwin", <PreloadData>{
    versions: {
        etwin: import.meta.env.PACKAGE_VERSION,
        chrome: process.versions.chrome,
        electron: process.versions.electron,
        node: process.versions.node,
        flash: getFlashVersion(),
    },
    onmessage(handler) {
        dealer.onmessage = ({ data }) => handler(data);
    },
    send(message) {
        dealer.postMessage(message);
    },
    env: "electron",
});
