import { getGames } from "../../common/etwinApi";
import { browserFetch } from "../../common/fetch";
import type { MainMessage } from "../../common/messages";

if (!window.etwin) {
    // preload data
    window.etwin = {
        versions: {
            etwin: import.meta.env.PACKAGE_VERSION,
            chrome: navigator.userAgent,
            electron: "<n/a>",
            node: "<n/a>",
            flash: null,
        },
        onmessage(handler) {
            const loggedHandler = (message: MainMessage) => {
                console.log("»ipc»", message[0], message[1]);
                handler(message);
            };
            setTimeout(() => {
                getGames(browserFetch).then(games =>
                    loggedHandler(["data:games", { games }]),
                );
            }, 500);
            loggedHandler(["data:update", { newVersion: "1.0.0" }]);
        },
        send([type, pl]) {
            console.log("«ipc«", type, pl);
        },
        env: "browser",
    };
}
