export function autobind<T extends Record<string, unknown>>(o: T): T {
    Object.entries(o).forEach(([k, v]: [keyof T, unknown]) => {
        if (typeof v === "function") {
            o[k] = v.bind(o);
        }
    });
    return o;
}
