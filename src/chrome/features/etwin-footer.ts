import m from "mithril";

const $Footer = `footer.
    .p-2
    .fs-2/5
    .etwin-fg-light-gray
    .etwin-bg-black
`;

const $Container = `.
    .grid.grid-cols-4.gap-2/5
    .etwin-maxw-704px
    .mx-auto
`;

export const EtwinFooter = (): m.Component => ({
    view: ({ children }) => m($Footer, m($Container, children)),
});
