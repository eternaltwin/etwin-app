import m from "mithril";

const $Main = `main.
    .flex-grow
    .box-sizing
    .px-3/2.pb-5/2
    .etwin-fg-light-gray
    .etwin-main-styles
`;

export const EtwinMain = (): m.Component => ({
    view: ({ children }) => m($Main, children),
});
