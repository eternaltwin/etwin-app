import m from "mithril";

const $Article = `article.
    .px-1/2.py-2/3
    .b.etwin-bc-dark-purple.bw-1px.bw-b-1/10.br-1/5
`;

export const EtwinBox = (): m.Component<{ class?: string }> => ({
    view: ({ attrs, children }) =>
        m($Article, { class: attrs.class }, children),
});
